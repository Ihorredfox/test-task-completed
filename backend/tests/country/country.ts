import chai, {assert, expect} from "chai";
import chaiHttp from "chai-http";
import app from '../../src/app';
import { Country } from '../../src/app/models/Country';
import db from "../../src/db/db";
import setupTests from "../setupTests";

const should = chai.should();
chai.use(chaiHttp)

beforeEach(async () => {
    await setupTests();
})

describe("Country API", () => {
    const fillCountries = async () => {
        const ua = await Country.create({name: "Kyiv", code: "UA", flag: "some_flag", lat: 0, lng: 0});
        const uk = await Country.create({name: "United Kingdom", code: "UK", flag: "some_flag", lat: 0, lng: 0});
        const usa = await Country.create({name: "USA", code: "US", flag: "some_flag", lat:0, lng: 0});
        return [ua.getDataValue('id'), uk.getDataValue('id'), usa.getDataValue('id')];
    };


    describe("GET", () => {
        it("can't find entity by id", async () => {
            const response = await chai.request(app)
                .get("/api/v1/country/random_id");
            const {status, body} = response;
            assert.equal(status, 500);
            assert.equal(body.msg, "Record with id random_id not found");
        });

        it("can find entity by id", async () => {
            const countriesIds = await fillCountries();
            const response = await chai.request(app)
                .get("/api/v1/country/"+countriesIds[0]);
            const {status, body} = response;

            assert.equal(status, 200);
            assert.hasAllKeys(body, ['id', 'name', 'code', 'createdAt', 'updatedAt', 'lat', 'lng', 'flag']);

            assert.equal(body.name, 'Kyiv');
            assert.equal(body.code, 'UA');
        });
    });

    describe("GET ALL", () => {
        it('Have records', async () => {
            await fillCountries();
            const response = await chai.request(app)
                    .get("/api/v1/country");
            const {status, body} = response;
            assert.equal(status, 200);
            assert.lengthOf(body.data, 3);
            body.data.forEach((item: Country) => {
                assert.hasAllKeys(item, ['id', 'name', 'code', 'createdAt', 'updatedAt', 'lat', 'lng', 'flag']);
            });
        });

        it('Have no records', async () => {
            const response = await chai.request(app)
                .get("/api/v1/country");
            const {status, body} = response;
            assert.equal(status, 200);
            assert.lengthOf(body.data, 0);
        });
    });

    describe("CREATE", () => {
        it('Can create new country', async () => {
            const payload = {
                name: "some_country",
                code: "SC",
                lat: 1,
                lng: 2,
                flag: "some_flag"
            };
            const response = await chai.request(app)
                .post("/api/v1/country")
                .send(payload);

            const {status, body} = response;
            const countries = await Country.findAll();

            assert.equal(status, 200);
            assert.hasAllKeys(body, ['id', 'name', 'code', 'createdAt', 'updatedAt', 'lat', 'lng', 'flag']);
            assert.lengthOf(countries, 1);
            assert.equal(payload.name, countries[0].getDataValue('name'));
            assert.equal(payload.code, countries[0].getDataValue('code'));
            assert.equal(body.id, countries[0].getDataValue('id'));
        });

        it("Can't create new country. Invalid payload", async () => {
            const payload = {
                name: "some_country",
                lat:0,
                lng:0,
                flag:"flag"
            };
            const response = await chai.request(app)
                .post("/api/v1/country")
                .send(payload);
            const {status, body} = response;
            assert.equal(status, 400);
            assert.deepEqual({msg: "The code field should not be empty"}, body);
        });

        it("Can't create new country. Record is already exist", async () => {
            const payload = {
                name: "some_country",
                code: "SC",
                flag: "some_flag",
                lat: 0,
                lng: 0
            };
            Country.create(payload);
            const response = await chai.request(app)
                .post("/api/v1/country")
                .send(payload);
            const {status, body} = response;

            assert.equal(status, 500);
        });

    });


    describe('UPDATE', () => {
        it('Can update country', async () => {
            const country = await Country.create({name: "some_country",code: "SC", flag: "some_flag", lat: 0, lng: 0});
            const countryId = country.getDataValue("id");
            const response = await chai.request(app)
                .put(`/api/v1/country/${countryId}`)
                .send({name: "updated_name", lat: 1, lng: 1, flag: "some_flag_updated"});
            const {status, body} = response;
            const updatedCountry = await Country.findByPk(countryId);

            assert.equal(status, 204);
            assert.deepEqual({}, body);
            assert.equal(updatedCountry?.getDataValue('name'), 'updated_name');
            assert.equal(updatedCountry?.getDataValue('lat'), 1);
            assert.equal(updatedCountry?.getDataValue('lng'), 1);
            assert.equal(updatedCountry?.getDataValue('flag'), 'some_flag_updated');
            assert.equal(updatedCountry?.getDataValue('code'), 'SC');


        });
        it("Can't update country. Empty payload", async () => {
            const country = await Country.create({name: "some_country",code: "SC", flag: "some_flag", lat: 0, lng: 0});
            const countryId = country.getDataValue("id");
            const response = await chai.request(app)
                .put(`/api/v1/country/${countryId}`)
                .send({});
            const {status, body} = response;

            assert.equal(status, 400);
            assert.deepEqual({msg: "Invalid value(s)"}, body);
        });
        it("Can't update country. Wrong data type", async () => {
            const country = await Country.create({name: "some_country",code: "SC", flag: "some_flag", lat: 0, lng: 0});
            const countryId = country.getDataValue("id");
            const response = await chai.request(app)
                .put(`/api/v1/country/${countryId}`)
                .send({name: {test: "test"}});
            const {status, body} = response;

            assert.equal(status, 400);
            assert.deepEqual({msg: "Invalid value(s)"}, body);
        });
        it("Can't update country. Record already exist", async () => {
            await Country.create({name: "some_country_1",code: "SC1", flag: "some_flag", lat: 0, lng: 0});
            const country = await Country.create({name: "some_country",code: "SC", flag: "some_flag", lat: 0, lng: 0});
            const countryId = country.getDataValue("id");
            const response = await chai.request(app)
                .put(`/api/v1/country/${countryId}`)
                .send({name: "some_country_1"});
            const {status, body} = response;

            assert.equal(status, 500);
            assert.deepEqual({msg: "Record with such name is already exist"}, body);
        });
    });


    describe('DELETE', () => {
        it('Can delete', async () => {
            const country = await Country.create({name: "some_country",code: "SC", flag: "some_flag", lat: 0, lng: 0});
            const countryId = country.getDataValue("id");
            const response = await chai.request(app)
                .delete(`/api/v1/country/${countryId}`)
            const {status, body} = response;
            
            const countries = await Country.findAll();

            assert.equal(status, 204);
            assert.deepEqual({}, body);
            assert.lengthOf(countries, 0);
        });
    })
    
});