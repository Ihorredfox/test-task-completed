import db from "../src/db/db";

export default async function() {
    await db.migrate(true);
}