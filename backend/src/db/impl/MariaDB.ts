import { ModelType, Sequelize } from 'sequelize';
import { Database } from '../abstract/Database';

export default class MariaDB extends Database {
    public async connect(): Promise<void> {
        const sequelize = this.engine;
        await sequelize.authenticate();
    }

    protected buildEngine(): Sequelize {
        return new Sequelize(
            this.credentials.database, 
            this.credentials.username,
            this.credentials.password,
            {
                host: this.credentials.host,
                dialect: "mariadb",
                port: this.credentials.port,
                logging: false
            },
        ); 
    }

    public getEngine(): Sequelize {
        return this.engine
    }

    public async migrate(force: boolean = false): Promise<void> {
        await this.engine.sync({force});
    }

    public async dropTables(): Promise<void> {
        await this.engine.drop();
    }

    public assignModels(models: Array<ModelType>): void {
        models.forEach((model) => this.engine.modelManager.addModel(model));
    }
}