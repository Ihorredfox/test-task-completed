import { ModelType, Sequelize } from 'sequelize';

export abstract class Database {
    protected credentials: DatabaseCredentials;
    protected engine: DatabaseEngine;

    public constructor(credentials: DatabaseCredentials) {
        this.credentials = credentials;
        this.engine = this.buildEngine();
    }

    public abstract connect(): Promise<void>;
    public abstract migrate(force: boolean): Promise<void>;
    protected abstract buildEngine(): DatabaseEngine;
    public abstract getEngine(): DatabaseEngine;
    public abstract dropTables(): Promise<void>;
    public abstract assignModels(models: Array<AssignableModel>): void;
}

export interface DatabaseCredentials {
    database: string;
    username: string;
    password: string;
    host: string;
    port: number;
}
type DatabaseEngine = Sequelize;
type AssignableModel = ModelType;