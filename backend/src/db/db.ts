import { DatabaseCredentials } from "./abstract/Database";
import MariaDB from './impl/MariaDB';

const testEnv = process.env["npm_config_test"];

const credentials: DatabaseCredentials = {
    database: process.env["MYSQL_DATABASE"] || "",
    username: process.env["MYSQL_USER"] || "",
    password: process.env["MYSQL_PASSWORD"] || "",
    host: (!testEnv ? process.env["MYSQL_HOST"] : process.env["MYSQL_TEST_HOST"]) || "",
    port: 3306
};

const db = new MariaDB(credentials);
export default db;