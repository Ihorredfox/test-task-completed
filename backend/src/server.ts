import app from "./app";
import {Database, DatabaseCredentials} from "./db/abstract/Database";
import db from "./db/db";
import { Country } from "./app/models/Country";

const port = process.env['PORT'] || 80;


export async function run(database: Database): Promise<void> {
    try {
        await database.connect();
        database.assignModels([Country]);
        await database.migrate(false);
        app.listen(port, () => {
            console.log("server is running");
        });
    } catch(e: any) {
        throw new Error(e);
    }
}
run(db);