import {Router} from "express";
import CountryController from "../app/controllers/CountryController";
import ErrorMiddleware from "../app/middleware/ErrorMiddleware";
import CountryValidator from "../app/validators/CountryValidator";

const router = Router();
const controller = new CountryController();
const errorMiddleware = new ErrorMiddleware();
const countryValidator = new CountryValidator();

router.get(
    '/:id',
    countryValidator.validateIdParam(),
    errorMiddleware.handleValidationError.bind(errorMiddleware),
    controller.get.bind(controller)
);
router.get(
    '/',
    controller.getAll.bind(controller)
);
router.post(
    '/',
    countryValidator.validateCreate(),
    errorMiddleware.handleValidationError.bind(errorMiddleware),
    controller.create.bind(controller)
);
router.put(
    '/:id',
    countryValidator.validateUpdate(),
    errorMiddleware.handleValidationError.bind(errorMiddleware),
    controller.update.bind(controller)
);
router.delete(
    '/:id',
    countryValidator.validateIdParam(),
    errorMiddleware.handleValidationError.bind(errorMiddleware),
    controller.delete.bind(controller)
);



export default router;