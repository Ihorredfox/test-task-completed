import { Database } from "../../db/abstract/Database";
import db from '../../db/db';
import axios from "axios";
import { Country, CountryCreateData } from '../models/Country';

interface ResponseItem {
    capitalInfo: {
        latLng: [number, number]
    },
    latLng: [number, number],
    flags: {
        png: string,
        svg: string
    },
    name: {
        common: string,
        official: string
    },
    latlng: [number, number],
    cioc: string,
    cca2: string
}

class Seeder {
    constructor(private db: Database) {
        this.db = db;
    }

    private async setupDatabase(): Promise<void> {
        this.db.assignModels([Country]);
        await this.db.migrate(false);
    }

    public async seed(): Promise<void> {
        console.log("Setting up database migration...");
        await this.setupDatabase();
        console.log("Database migration success");
        console.log("Starting seeding...");
        const data: CountryCreateData[] = await this.seedData();
        Country.bulkCreate(data);
        console.log("Seeding completed!");
    }


    private async seedData(): Promise<CountryCreateData[]> {
        const data: ResponseItem[] = await this.requestData();
        return data.map((item: ResponseItem) => {
            const [lat, lng] = item.latLng ?? item.capitalInfo.latLng ?? item.latlng;
            return {
                name: item.name.official,
                code: item.cioc ?? item.cca2,
                flag: item.flags.png,
                lat,
                lng
            }
        });
    }

    private async requestData(): Promise<ResponseItem[]> {
        const response = await axios.get('https://restcountries.com/v3.1/all');
        return await response.data as ResponseItem[];
    }
}

const seeder = new Seeder(db);
seeder.seed();
