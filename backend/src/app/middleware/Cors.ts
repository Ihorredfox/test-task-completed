import { Request, Response, NextFunction } from 'express';

export default class Cors {
	public static handle(req: Request, res: Response, next: NextFunction): void {
        res.append('Access-Control-Allow-Origin', ['*']);
        res.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
        res.append('Access-Control-Allow-Headers', 'Content-Type');
        next();
	}
}