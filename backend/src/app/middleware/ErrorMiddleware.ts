import { Request, Response, NextFunction } from 'express';
import { validationResult } from 'express-validator';

export default class ErrorMiddleware {
	public handleValidationError(req: Request, res: Response, next: NextFunction): void {
		const error = validationResult(req);
		if (!error.isEmpty()) {
			const errData = error.array()[error.array().length - 1];
			res.status(400).json({msg: errData.msg});
		} else {
			next();
		}
	}
}