import {Request, Response} from "express";
import { CountryCreateData, CountryUpdateData } from "../models/Country";
import CountryService from '../services/CountryService';

export default class CountryController {

    public constructor(
        private countriesService: CountryService = new CountryService()
    ) {
        this.countriesService = countriesService;
    }

    public async get(req: Request, res: Response): Promise<void> {
        try {
            const entity = await this.countriesService.get(req.params['id'])
            res.status(200).json(entity);
        } catch (e: any) {
            res.status(500).json({msg: e.message});
        }
    }

    public async getAll(req: Request, res: Response): Promise<void> {
        try {
            const entities = await this.countriesService.getAll();
            res.status(200).json({data: entities});
        } catch (e: any) {
            res.status(500).json({msg: e.message});
        }
    }

    public async create(req: Request, res: Response): Promise<void> {
        try {
            const entity = await this.countriesService.create(req.body as CountryCreateData);
            res.status(200).json(entity);
        } catch(e: any) {
            res.status(500).json({msg: e.message});
        }
    }

    public async update(req: Request, res: Response): Promise<void> {
        try {
            await this.countriesService.update(req.params["id"], req.body as CountryUpdateData);
            res.status(204).json();
        } catch(e: any) {
            res.status(500).json({msg: e.message});
        }
    }

    public async delete(req: Request, res: Response): Promise<void> {
        try {
            await this.countriesService.delete(req.params["id"]);
            res.status(204).json();
        } catch(e: any) {
            res.status(500).json({msg: e.message});
        }
    }
}