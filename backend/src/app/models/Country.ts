import { DataTypes, Model, Sequelize } from "sequelize";
import db from "../../db/db";

interface CountryAttributes {
    id?: string,
    name: string,
    code: string,
    flag: string,
    lat: number,
    lng: number,
    createdAt?: Date,
    updatedAt?: Date
}

export interface CountryCreateData {
    name: string,
    code: string,
    flag: string,
    lat: number,
    lng: number
}
export interface CountryUpdateData {
    name?: string,
    code?: string,
    lat?: number,
    lng?: number
}

export class Country extends Model<CountryAttributes> {}


Country.init(
    {
        id: {
            type: DataTypes.UUID,
            primaryKey: true,
            allowNull: false,
            unique: true,
            defaultValue: DataTypes.UUIDV4
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        },
        code: {
            type: DataTypes.STRING,
            allowNull: false
        },
        flag: {
            type: DataTypes.STRING,
            allowNull: false
        },
        lat: {
            type: DataTypes.FLOAT,
            allowNull: false
        },
        lng: {
            type: DataTypes.FLOAT,
            allowNull: false
        }
    },
    {
        sequelize: db.getEngine(),
        tableName: "countries"
    }
)