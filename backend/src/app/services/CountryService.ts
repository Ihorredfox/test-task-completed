import { Country, CountryCreateData, CountryUpdateData } from "../models/Country";


export default class CountryService {
    public constructor() {}

    public async get(id: string): Promise<Country> {
        const entity = await Country.findByPk(id);
        if(!entity) {
            throw new Error(`Record with id ${id} not found`);
        }
        return entity;
    }

    public async getAll(): Promise<Country[]> {
        const entities = await Country.findAll();
        return entities;
    }

    public async create(data: CountryCreateData): Promise<Country> {
        const entity = await Country.create(data);
        return entity;
    }

    public async update(id: string, data: CountryUpdateData): Promise<void> {
        if(data.name) {
            const entity = await Country.findOne({where: {name: data.name}})
            if(entity) {
                throw new Error("Record with such name is already exist");
            }
        }
        await Country.update(data, {where: {id}});
    }

    public async delete(id: string): Promise<void> {
        await Country.destroy({where: {
            id
        }});
    }
}