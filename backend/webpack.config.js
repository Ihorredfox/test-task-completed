const path = require('path');
const nodeExternals = require('webpack-node-externals');

module.exports = {
  entry: {
    bundle: './src/server.ts',
    seeder: './src/app/command/Seeder.ts'
  },
  target: 'node',
  externals: [nodeExternals()],
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
    ],
  },
  resolve: {
    extensions: ['.ts', '.js'],
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'dist'),
  },
};